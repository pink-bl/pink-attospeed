#!/usr/bin/python3

import epics
import numpy as np
import threading
import time

eve = threading.Event()

def onchange(*args, **kargs):
  eve.set()

## Log start
print("[{}] ** attospeed **".format(time.asctime()))

## epics channels
print("Connecting epics channels...")
## attocube
spdpv = epics.PV("PINK:ANC01:ACT0:calcspeed", auto_monitor=True, callback=onchange)
#spdpv = epics.PV("PINK:ANC01:ACT0:speedavg", auto_monitor=True, callback=onchange)
mdonepv = epics.PV("PINK:ANC01:ACT0:IN_TARGET", auto_monitor=True)
mpospv = epics.PV("PINK:ANC01:ACT0:POSITION", auto_monitor=True)
freqoutpv = epics.PV("PINK:ANC01:ACT0:CMD:FREQ", auto_monitor=False)
basefreqpv = epics.PV("PINK:ANC01:ACT0:AuxFreq1", auto_monitor=True)
## hmi
enablepv = epics.PV("PINK:ATSPD:enable", auto_monitor=True)
navgpv = epics.PV("PINK:ATSPD:navg", auto_monitor=True)
spdtargetpv = epics.PV("PINK:ATSPD:spdtarget", auto_monitor=True)
deadbandpv = epics.PV("PINK:ATSPD:deadband", auto_monitor=True)
kppv = epics.PV("PINK:ATSPD:pid:kp", auto_monitor=True)
kdpv = epics.PV("PINK:ATSPD:pid:kd", auto_monitor=True)
kipv = epics.PV("PINK:ATSPD:pid:ki", auto_monitor=True)
maxsteppv = epics.PV("PINK:ATSPD:maxstep", auto_monitor=True)

freqfwdpv = epics.PV("PINK:ATSPD:freq:fwd", auto_monitor=True)
freqrevpv = epics.PV("PINK:ATSPD:freq:rev", auto_monitor=True)
pidoutpv = epics.PV("PINK:ATSPD:pid:out", auto_monitor=False)
avgsmoothpv = epics.PV("PINK:ATSPD:avgsmooth", auto_monitor=False)
directionpv = epics.PV("PINK:ATSPD:direction", auto_monitor=False)

## delay time to connect
time.sleep(2)

## init vars
state=0
freqpvarray = [freqfwdpv, freqrevpv]
avghist = []
tlast = time.time()
errlast = 0.0
errsum = 0.0
state=0
Kout=0.0
cnt=0
dir=0

## main loop
print("Listening...")
while(True):
  eve.wait()
  avghist.append(spdpv.value)
  navg = int(abs(navgpv.value))
  avghist = avghist[-navg:]
  avgsmooth = np.mean(avghist)

  ## wait for motion
  if mdonepv.value<1.0:
    if state==0:
      cnt+=1
      if cnt>=(2*navg):
        state=1
        cnt=0
        errsum=0
        if avgsmooth>0:
          dir=0
          freqpv = freqpvarray[0]
        else:
          dir=1
          freqpv = freqpvarray[1]
    elif state==1:
      ## pidcalc
      tnow=time.time()
      dt=tnow-tlast
      tlast=tnow
      # err calc
      err = spdtargetpv.value - abs(avgsmooth)
      errd =(err-errlast)/dt
      errlast = err
      # Kout calc
      if abs(err) <= deadbandpv.value:
        errsum=0.0
        Kout=0.0
      else:
        errsum += err
        if kipv.value>0.0:
          kimax = maxsteppv.value/kipv.value
          if abs(errsum)>kimax:
            errsum = np.sign(errsum)*kimax
        Kout = (kppv.value*err) + (kdpv.value*errd) + (kipv.value*errsum)
        if abs(Kout)>maxsteppv.value:
          Kout = np.sign(Kout)*maxsteppv.value

        if enablepv.value>0.0:
          # calc new freq
          lastfreq = freqpv.value
          newfreq = round(lastfreq + Kout)
          # keep within freq domain
          if newfreq > basefreqpv.value:
            newfreq = basefreqpv.value
          if newfreq < 1.0:
            newfreq = 1.0
          # update freq
          if newfreq != lastfreq:
            freqpv.put(newfreq)
            freqoutpv.put(newfreq)
  else:
    state=0
    Kout=0.0

  # update pvs
  avgsmoothpv.put(avgsmooth)
  pidoutpv.put(round(Kout))
  directionpv.put(state*(state+dir))
  eve.clear()

print("OK")


